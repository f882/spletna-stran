<div class="form">
    <div id="register">
        <?php
        if (isset($message_display))
        {
            echo "<div class='message'>".$message_display."</div>"; 
        }
        
        $action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/user_authentication/register/'.$user_role ;
        echo form_open($action);

        if (isset($error_message)) 
        {
            echo '<div class="error_msg">'.
                    $error_message.
                 '</div>';
        }

        echo '<div id="register_data">
                <label class="label">Uporabniško ime :</label>
                <input class="input" type="text" name="username" id="name"><br>

                <label class="label">E-pošta :</label>
                <input class="input" type="email" name="email_value" id="name"><br>

                <label class="label">Geslo :</label>
                <input class="input" class="input" type="password" name="password" id="password"><br>';
		echo '</div>';

		if ($user_role == 'venue') 
				{
					echo '<div id="register_data_venue">';
					echo '<label class="label" for="name">Ime gostišča :</label>';
					echo '<input class="input" type="input" name="name" /> <br/> ';

					echo '<label class="label" for="address">Naslov gostišča :</label>';
					echo '<input class="input" type="input" name="address" /> <br/>';
					
					echo '<label class="label" for="location">Regija :</label>'; 
					$options = array(
					        'Pomurska'           => 'Pomurska regija',
					        'Podravska'          => 'Podravska regija',
					        'Koroska'            => 'Koroška regija',
					        'Savinjska'          => 'Savinjska regija',
					        'Zasavska'           => 'Zasavska regija',
					        'Posavska'           => 'Posavska regija',
					        'Jugovzhodna'        => 'Jugovzhodna Slovenija',
					        'Osrednjeslovenska'  => 'Osrednjeslovenska regija',
					        'Gorenjska'          => 'Gorenjska regija',
					        'Primorsko'          => 'Primorsko-notranjska regija',
					        'Goriška'            => 'Goriška regija',
					        'Obalno'             => 'Obalno-kraška regija',      
					);
					echo form_dropdown('location', $options, 'pomurska', 'class="input"');
					echo '<br>';

					echo '<label class="label" for="food">Hrana :</label>';
					$options = array(
					        'Neopredeljeno'   => 'Neopredeljeno',
					        'Pizze'           => 'Pizze',
					        'Burgerji'        => 'Burgerji',
					        'Testenine'       => 'Testenine',
					        'Zar'             => 'Jedi z žara',
					        'Morska'          => 'Morska hrana',
					        'Mehiska'         => 'Mehiška hrana',
					        'Kitajska'        => 'Kitajska hrana',    
					);
					echo form_dropdown('food', $options, 'neopredeljeno', 'class="input"');
					echo '<br>';

					echo '<label class="label" for="price">Cenovni razred :</label>';
					$options = array(
					        'Poceni'    => 'Poceni',
					        'Srednje'   => 'Srednje',
					        'Drago'     => 'Drago',
					);
					echo form_dropdown('price', $options, 'poceni', 'class="input"');
					echo '<br>';

					echo '<label class="label" for="company">Podjetje :</label>';
					echo '<input class="input" type="input" name="company" /> <br/>';

					echo '<label class="label" for="open">Odprto :</label>';
					echo '<input id="working" class="input" type="number" name="open" min="0" max="23"/><p id="zerozero">.00</p> <br/> ';

					echo '<label class="label" for="close">Zaprto :</label>';
					echo '<input id="working" class="input" type="number" name="close" min="0" max="23"/><p id="zerozero">.00</p> <br/>';
					echo '</div>';
				}

        echo '<div class="registration_links">
                <input id="submit" type="submit" value=" REGISTRACIJA " name="submit"><br>
              </div>';

        echo form_close();

        ?>
    </div>
</div>

