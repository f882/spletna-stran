<div class="form">
    <div id="login">
        <?php 
        if (isset($message_display))
        {
            echo "<div class='message'>".$message_display."</div>"; 
        }
        
        $action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/user_authentication/signin' ;
        echo form_open($action);

        if (isset($error_message)) 
        {
            echo '<div class="error_msg">'.
                    $error_message.
                 '</div>';
        }

        echo '<div id="input_data">
                <label class="label">Uporabniško ime :</label>
                <input class="input" type="text" name="username" id="name"><br>

                <label class="label">Geslo :</label>
                <input class="input" class="input" type="password" name="password" id="password"><br>
              </div>';

        echo '<div>
                <input id="submit" type="submit" value=" PRIJAVA " name="submit"><br>
              </div>';

        echo '<div class="registration_links">
                <p>Če še nimate računa se registrirajte</p>
                <a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/user_authentication/signup/venue">Registriraj gostišče</a> <br>
                <a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/user_authentication/signup/normal_user">Registriraj se kot navaden uporabnik</a>
              </div>';

        echo form_close();
        ?>
    </div>
</div>