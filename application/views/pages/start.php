
<div class="main" id="search">
  <h3 >NAJDI GOSTIŠČE</h3>

  <form method="get" action="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venues" id="filterform">
    <label for="location">LOKACIJA:</label>
    <select id="search_select" name="location" id="location" form="filterform">
      <option value="Pomurska">Pomurska regija</option>
      <option value="Podravska">Podravska regija</option>
      <option value="Koroska">Koroška regija</option>
      <option value="Savinjska">Savinjska regija</option>
      <option value="Zasavska">Zasavska regija</option>
      <option value="Posavska">Posavska regija</option>
      <option value="Jugovzhodna">Jugovzhodna Slovenija</option>
      <option value="Osrednjeslovenska">Osrednjeslovenska regija</option>
      <option value="Gorenjska">Gorenjska regija</option>
      <option value="Primorsko">Primorsko-notranjska regija</option>
      <option value="Goriška">Goriška regija</option>
      <option value="Obalno">Obalno-kraška regija</option>
    </select>

    <label for="food">HRANA:</label>
    <select id="search_select" name="food" id="food" form="filterform">
      <option value="Neopredeljeno">Ni pomembno</option>
      <option value="Pizze">Pizze</option>
      <option value="Burgerji">Burgerji</option>
      <option value="Testenine">Testenine</option>
      <option value="Zar">Jedi z žara</option>
      <option value="Morska">Morska hrana</option>
      <option value="Mehiska">Mehiška hrana</option>
      <option value="Kitajska">Kitajska hrana</option>
    </select>

    <label for="price">CENOVNI RAZRED:</label>
    <select id="search_select" name="price" id="price" form="filterform">
      <option value="Poceni">Poceni</option>
      <option value="Srednje">Srednje</option>
      <option value="Drago">Drago</option>
    </select>

    <input id="find" type="submit" name="submit" value="NAJDI GOSTIŠČA" />
  </form>
</div>

<div class="main">
  <h3>AKTUALNI DOGODKI</h3>
  <?php foreach ($dogodki as $dogodek): ?>
    <hr>
    <h4><?php echo $dogodek['ime']; ?></h4>

    <p>
      Vrsta: <?php echo $dogodek['vrsta']; ?><br>
      Datum: <?php echo $dogodek['datum']; ?><br>
      Lokacija: <?php echo $dogodek['lokacija']; ?><br>
      <a id="organizator" href="<?php echo 'http://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_single_venue/'.$dogodek['id_organizatorja']; ?>">ORGANIZATOR</a>
    </p>
    
  <?php endforeach; ?>
</div>

