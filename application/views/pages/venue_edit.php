<div class="main">
		<?php
	echo '<h2>UREDI</h2>';
	 

	if ($to_be_edited == 'basic_info') 
	{	
		echo validation_errors(); 
		$attributes = array('class' => 'editform', 'id' => 'editform'); 
		$action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_edit/'.$id.'/'.$to_be_edited ;
		echo form_open($action, $attributes); 
			echo '<label id="edit_label" for="name">Ime</label>';
			echo '<input type="input" name="name" value="'.$gostisce["ime"].'" /> <br/> ';

			echo '<label id="edit_label" for="address">Naslov</label>';
			echo '<input type="input" name="address" value="'.$gostisce["naslov"].'" /> <br/>';
			
			echo '<label id="edit_label" for="location">Regija</label>'; 
			$options = array(
			        'Pomurska'           => 'Pomurska regija',
			        'Podravska'          => 'Podravska regija',
			        'Koroska'            => 'Koroška regija',
			        'Savinjska'          => 'Savinjska regija',
			        'Zasavska'           => 'Zasavska regija',
			        'Posavska'           => 'Posavska regija',
			        'Jugovzhodna'        => 'Jugovzhodna Slovenija',
			        'Osrednjeslovenska'  => 'Osrednjeslovenska regija',
			        'Gorenjska'          => 'Gorenjska regija',
			        'Primorsko'          => 'Primorsko-notranjska regija',
			        'Goriška'            => 'Goriška regija',
			        'Obalno'             => 'Obalno-kraška regija',      
			);
			echo form_dropdown('location', $options, $gostisce['regija'], 'form="editform"');
			echo '<br>';

			echo '<label id="edit_label" for="food">Hrana</label>';
			$options = array(
			        'Neopredeljeno'   => 'Neopredeljeno',
			        'Pizze'           => 'Pizze',
			        'Burgerji'        => 'Burgerji',
			        'Testenine'       => 'Testenine',
			        'Zar'             => 'Jedi z žara',
			        'Morska'          => 'Morska hrana',
			        'Mehiska'         => 'Mehiška hrana',
			        'Kitajska'        => 'Kitajska hrana',    
			);
			echo form_dropdown('food', $options, $gostisce['hrana'], 'form="editform"');
			echo '<br>';

			echo '<label id="edit_label" for="price">Cena</label>';
			$options = array(
			        'Poceni'    => 'Poceni',
			        'Srednje'   => 'Srednje',
			        'Drago'     => 'Drago',
			);
			echo form_dropdown('price', $options, $gostisce['cena'], 'form="editform"');
			echo '<br>';

			echo '<label id="edit_label" for="company">Podjetje</label>';
			echo '<input type="input" name="company" value="'.$gostisce["naziv_podjetja"].'" /> <br/>';

			echo '<input id="submit" type="submit" name="submit" value="SHRANI" />';
		echo '</form>';
	}
	elseif ($to_be_edited == 'working_hours')
	{
		echo validation_errors(); 
		$attributes = array('class' => 'editform', 'id' => 'editform'); 
		$action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_edit/'.$id.'/'.$to_be_edited ;
		echo form_open($action, $attributes); 
			echo '<label id="edit_label" for="open">Odprto</label>';
			echo '<input type="number" name="open" value="'.$odpiralni_casi['odprto'].'" />:00 <br/> ';

			echo '<label id="edit_label" for="close">Zaprto</label>';
			echo '<input type="number" name="close" value="'.$odpiralni_casi['zaprto'].'" />:00 <br/>';

			echo '<input id="submit" type="submit" name="submit" value="SHRANI" />';
		echo '</form>';
	}
	elseif ($to_be_edited == 'price_list') 
	{
		echo validation_errors(); 
		$attributes = array('class' => 'editform', 'id' => 'editform'); 
		$action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$id.'/'.$to_be_edited ;
		echo form_open($action, $attributes); 
			echo '<label id="edit_label" for="dish">Ime jedi</label>';
			echo '<input type="input" name="dish"  /> <br/> ';

			echo '<label id="edit_label" for="price">Cena</label>';
			echo '<input type="input" name="price"  />€ <br/>';

			echo '<input id="submit" type="submit" name="submit" value="USTVARI" />';
		echo '</form>';
	}
	elseif ($to_be_edited == 'events') 
	{
		echo validation_errors(); 
		$attributes = array('class' => 'editform', 'id' => 'editform'); 
		$action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$id.'/'.$to_be_edited ;
		echo form_open($action, $attributes); 
			echo '<label id="edit_label" for="name">Ime dogodka</label>';
			echo '<input type="input" name="name"  /> <br/> ';

			echo '<label id="edit_label" for="location">Lokacija</label>';
			echo '<input type="input" name="location"  /> <br/>';

			echo '<label id="edit_label" for="date">Datum</label>';
			echo '<input type="date" name="date"  /> <br/>';

			echo '<label id="edit_label" for="description">Vrsta dogodka</label>';
			echo '<input type="input" name="description"  /> <br/>';

			echo '<label id="edit_label" for="max_guests">Največje število gostov</label>';
			echo '<input type="number" name="max_guests"  /> <br/>';

			echo '<input id="submit" type="submit" name="submit" value="USTVARI" />';
		echo '</form>';
	}
	elseif ($to_be_edited == 'tables') 
	{
		echo validation_errors(); 
		$attributes = array('class' => 'editform', 'id' => 'editform'); 
		$action = 'https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$id.'/'.$to_be_edited ;
		echo form_open($action, $attributes); 
			echo '<label id="edit_label" for="table_num">Številka mize</label>';
			echo '<input type="number" name="table_num"  /> <br/> ';

			echo '<label id="edit_label" for="max_guests">Največje število oseb</label>';
			echo '<input type="number" name="max_guests"  /> <br/> ';

			echo '<input id="submit" type="submit" name="submit" value="USTVARI" />';
		echo '</form>';
	}
	?>

</div>


