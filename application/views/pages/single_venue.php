<div class="main">
        

<?php
if ($this->session->userdata('user_id') == $gostisce['id'])
{ 
        echo '<h2 class="single_venue_sections">'.$gostisce['ime'].'</h2>';
        echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_edit/'.$gostisce['id'].'/basic_info">UREDI</a>'.'<br>';
        echo 'Naslov: '.$gostisce['naslov'].'<br>';
        echo 'Hrana: '.$gostisce['hrana'].'<br>';
        echo 'Cenovni razred: '.$gostisce['cena'].'<br>';
        echo '<hr>';

        echo '<h2 class="single_venue_sections">ODPIRALNI ČASI</h2>';
        echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_edit/'.$gostisce['id'].'/working_hours">UREDI</a>'.'<br>';
        echo 'Odprto: '.$odpiralni_casi['odprto'].':00 <br>';
        echo 'Zaprto: '.$odpiralni_casi['zaprto'].':00 <br>';
        echo '<hr>';

        echo '<h2 class="single_venue_sections">CENIK</h2>';
        echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$gostisce['id'].'/price_list">DODAJ JED</a>'.'<br>';
        foreach ($cenik as $jed):
                echo 'Ime jedi: '.$jed['ime_jedi'].'';
                echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_delete/dish/'.$jed['id'].'">IZBRIŠI</a>'.'<br>';
                echo 'Cena: '.$jed['cena'].' €<br><br>';
        endforeach;
        echo '<hr>';

        echo '<h2 class="single_venue_sections">DOGODKI</h2>';
        echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$gostisce['id'].'/events">DODAJ DOGODEK</a>'.'<br>';
        foreach ($dogodki as $dogodek):
                echo 'Ime dogodka: '.$dogodek['ime'].'';
                echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_delete/event/'.$dogodek['id'].'">IZBRIŠI</a>'.'<br>';
                echo 'Datum: '.$dogodek['datum'].'<br><br>';
        endforeach;
        echo '<hr>';

        echo '<h2 class="single_venue_sections">MIZE</h2>';
        echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_create/'.$gostisce['id'].'/tables">DODAJ MIZO</a>'.'<br>';
        foreach ($mize as $miza): 
                echo 'Številka mize: '. $miza['st_mize'] .'';
                echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_venue_delete/table/'.$miza['id_mize'].'">IZBRIŠI</a>'.'<br>';
                echo 'Največje število oseb: '. $miza['max_st_oseb'] .'<br><br>';
                
        endforeach;
        echo '<hr>'; 

        echo '<h2 class="single_venue_sections">REZERVACIJE</h2><br>';
        foreach ($rezervacije as $rezervacija): 
                echo 'Številka mize: '. $rezervacija['st_mize'] .'<br>';
                echo "Ura: ". $rezervacija['ura'] .':00<br>';
                echo "Na ime: ". $rezervacija['uporabnisko_ime'] .'<br><br>';
                //echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/">IZBRIŠI</a>'.'<br>';
                //echo "<hr>";
        endforeach;
        echo '<hr>';
}
else
{
        echo '<h2 class="single_venue_sections">'.$gostisce['ime'].'</h2><br>';
        echo 'Naslov: '.$gostisce['naslov'].'<br>';
        echo 'Hrana: '.$gostisce['hrana'].'<br>';
        echo 'Cenovni razred: '.$gostisce['cena'].'<br>';
        echo '<hr>';

        echo '<h2 class="single_venue_sections">ODPIRALNI ČASI</h2><br>';
        echo 'Odprto: '.$odpiralni_casi['odprto'].':00 <br>';
        echo 'Zaprto: '.$odpiralni_casi['zaprto'].':00 <br>';
        echo '<hr>';

        echo '<h2 class="single_venue_sections">CENIK</h2><br>';
        foreach ($cenik as $jed):
                echo 'Ime jedi: '.$jed['ime_jedi'].'<br>';
                echo 'Cena: '.$jed['cena'].' €<br><br>';
                //echo '<hr>';
        endforeach;
        echo '<hr>';

        echo '<h2 class="single_venue_sections">DOGODKI</h2><br>';
        foreach ($dogodki as $dogodek):
                echo 'Ime dogodka: '.$dogodek['ime'].'<br>';
                echo 'Datum: '.$dogodek['datum'].'<br><br>';
                //echo '<hr>';
        endforeach;
        echo '<hr>';

        if ($this->session->userdata('role') == 'normal_user')
        {
                echo '<h2 class="single_venue_sections">PROSTE REZERVACIJE</h2><br>';
                echo 'Za rezervacijo izberite in pritisnite željen termin.<br>';
                echo 'Rezervacije so možne samo za tekoči dan.<br><br>';
                if ($proste_rezervacije == FALSE) 
                {
                        echo 'Rezervacije trenutno niso možne!<br><br>';
                }
                else
                {
                        foreach ($proste_rezervacije as $termin):
                                echo 'Številka mize: '.$termin['st_mize'].'';
                                echo '<a href="https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_reserve_slot/'.$termin['st_mize'].'/'.$termin['ura'].'/'.$gostisce['id'].'/0">REZERVIRAJ</a>'.'<br>';
                                echo 'Največje število oseb: '.$termin['max_st_oseb'].'<br>';
                                echo 'Ura: '.$termin['ura'].':00-'.(intval($termin['ura'])+1).':00<br><br>';
                               
                        endforeach;
                }
        }
        
}

?>

</div>














