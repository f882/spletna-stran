<?php
class User_authentication_model extends CI_Model {

    public function __construct(){
        $this->load->database();
    }

    public function registration_insert($user_role, $data_user_login, $data_venue, $data_working_hours) 
    {
        $condition = "user_name =" . "'" . $data_user_login['user_name'] . "'";
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) 
        {
            $this->db->insert('user_login', $data_user_login);
            if ($this->db->affected_rows() > 0) 
            {
                if ($user_role == 'venue')
                {
                    $user_info = $this->read_user_information($data_user_login['user_name']);
                    $user_id = $user_info[0]->user_id;
                    $data_venue['id'] = $user_id;
                    $data_working_hours['id_gostisca'] = $user_id;
                    $this->db->insert('gostisce', $data_venue);
                    $this->db->insert('odpiralni_casi', $data_working_hours);

                }
                return true;
            }
        } 
        else 
        {
            return false;
        }
    }

    public function login($data) 
    {

        $condition = "user_name =" . "'" . $data['username'] . "' AND " . "user_password =" . "'" . $data['password'] . "'";
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    public function read_user_information($username) 
    {
        $condition = "user_name =" . "'" . $username . "'";
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) 
        {
            return $query->result();
        } 
        else 
        {
            return false;
        }
    }
}
