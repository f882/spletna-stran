<?php
class Main_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
                $this->load->helper('url_helper');
        }


        public function get_venues()
        {
                $regija = $_REQUEST['location'];
                $hrana = $_REQUEST['food'];
                $cena = $_REQUEST['price'];
                
                if ($hrana == 'Neopredeljeno') {
                        $query = $this->db->get_where('gostisce', array('regija' => $regija, 'cena' => $cena));
                        return $query->result_array();
                }else{
                        $query = $this->db->get_where('gostisce', array('regija' => $regija, 'hrana' => $hrana, 'cena' => $cena));
                        return $query->result_array();
                }
                
        }

        public function get_events()
        {
                $date = date("Y\-m\-d");
                $q = "SELECT * FROM dogodek WHERE datum >= '".$date."' ORDER BY datum";
                $query = $this->db->query($q);
                return $query->result_array();
               

        }

        public function get_venue_info($id)
        {
                $query = $this->db->get_where('gostisce', array('id' => $id));
                return $query->row_array();
        }

        public function get_venue_pricelist($id)
        {
                $query = $this->db->get_where('jedi', array('id_gostisca' => $id));
                return $query->result_array();
        }

        public function get_venue_workhours($id)
        {
                $query = $this->db->get_where('odpiralni_casi', array('id_gostisca' => $id));
                return $query->row_array();
        }

        public function get_venue_events($id)
        {
                $query = $this->db->get_where('dogodek', array('id_organizatorja' => $id));
                return $query->result_array();
        }

        public function get_venue_free_slots($id, $odpiralni_casi)
        {
                if (intval(date("H")) >= intval($odpiralni_casi['zaprto'])) 
                {
                        return FALSE;
                }

                if (intval($odpiralni_casi['odprto']) < intval(date("H"))) 
                {
                        $zacetek = date("H");
                }else{
                        $zacetek = $odpiralni_casi['odprto'];         
                }
                $konec = $odpiralni_casi['zaprto'];

                $mize = "SELECT st_mize, max_st_oseb
                         FROM mize
                         WHERE id_gostisca = $id";

                $ure = "SELECT ura
                        FROM ure
                        WHERE ura BETWEEN $zacetek AND $konec";

                $q =   "SELECT TEMP.st_mize, TEMP.ura, TEMP.max_st_oseb
                        FROM   (SELECT M.st_mize, U.ura, M.max_st_oseb
                                FROM ($mize) as M, ($ure) AS U) AS TEMP
                        LEFT JOIN rezervacije ON (TEMP.st_mize=rezervacije.st_mize AND TEMP.ura=rezervacije.ura)
                        WHERE rezervacije.st_mize IS NULL AND rezervacije.ura IS NULL
                        ORDER BY TEMP.st_mize, TEMP.ura ASC;";

                $query = $this->db->query($q);
                return $query->result_array();
        }

        public function get_reserved_slots($id)
        {
                $query = $this->db->get_where('rezervacije', array('id_gostisca' => $id));
                return $query->result_array();
        }

        public function get_tables($id)
        {
                $query = $this->db->get_where('mize', array('id_gostisca' => $id));
                return $query->result_array();
        }

        public function save_edited($id, $section)
        {
                if ($section == 'basic_info')
                {
                        $ime = $this->input->post('name');
                        $naslov = $this->input->post('address');
                        $regija = $this->input->post('location');
                        $hrana = $this->input->post('food');
                        $cena = $this->input->post('price');
                        $naziv_podjetja = $this->input->post('company');

                        $data = array(
                                'ime' => $ime,
                                'naslov' => $naslov,
                                'regija' => $regija,
                                'hrana' => $hrana,
                                'cena' => $cena,
                                'naziv_podjetja' => $naziv_podjetja,
                        );

                        $this->db->where('id', $id);
                        $this->db->update('gostisce', $data);
                }
                elseif ($section == 'working_hours') 
                {
                        $odprto = $this->input->post('open');
                        $zaprto = $this->input->post('close');
                        $data = array(
                                'odprto' => $odprto,
                                'zaprto' => $zaprto
                        );
                        $this->db->where('id_gostisca', $id);
                        $this->db->update('odpiralni_casi', $data);
                }
        }

        public function create($id, $section)
        {
                if ($section == 'price_list')
                {
                        $ime_jedi = $this->input->post('dish');
                        $cena = $this->input->post('price');
                        $this->db->insert('jedi', array('ime_jedi' => $ime_jedi, 'cena' => $cena, 'id_gostisca' => $id));
                }
                elseif ($section == 'events')
                {
                        $ime = $this->input->post('name');
                        $lokacija = $this->input->post('location');
                        $datum = $this->input->post('date');
                        $vrsta = $this->input->post('description');
                        $max_st_gostov = $this->input->post('max_guests');
                        
                        $data = array(
                                'ime' => $ime,
                                'lokacija' => $lokacija,
                                'datum' => $datum,
                                'vrsta' => $vrsta,
                                'max_st_gostov' => $max_st_gostov,
                                'id_organizatorja' => $id
                        );
                        $this->db->insert('dogodek', $data);
                }
                elseif ($section == 'tables')
                {
                        $st_mize = $this->input->post('table_num');
                        $max_st_oseb = $this->input->post('max_guests');
                        $this->db->insert('mize', array('st_mize' => $st_mize, 'max_st_oseb' => $max_st_oseb, 'id_gostisca' => $id));
                }
        }

        public function get_user_reservations($username)
        {
                $query = $this->db->get_where('rezervacije', array('uporabnisko_ime' => $username));
                return $query->result_array();
        }

        public function delete($to_delete, $id)
        {
                if ($to_delete == 'dish')
                {
                        $this->db->delete('jedi', array('id' => $id));
                }
                elseif ($to_delete == 'event')
                {
                        $this->db->delete('dogodek', array('id' => $id));
                }
                elseif ($to_delete == 'table')
                {
                        $this->db->delete('mize', array('id_mize' => $id));
                }
        }

        public function make_reservation($reservation_data)
        {
                $this->db->insert('rezervacije', $reservation_data);
        }

}
