<?php
class Main extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->library('session');
                $this->load->model('main_model');
                $this->load->helper('form');
                $this->load->library("form_validation");
                $this->load->helper('url_helper');
        }

        public function view_start()
        {;
                $data['dogodki'] = $this->main_model->get_events();

                $this->load->view('templates/header');
                $this->load->view('pages/start', $data);
                $this->load->view('templates/footer');
        }


        public function view_venues()
        {
                $data['gostisca'] = $this->main_model->get_venues();

                $this->load->view('templates/header');
                $this->load->view('pages/venues', $data);
                $this->load->view('templates/footer');
        }

        public function view_single_venue($id)
        {
                $data['gostisce'] = $this->main_model->get_venue_info($id);
                $data['cenik'] = $this->main_model->get_venue_pricelist($id);
                $data['odpiralni_casi'] = $this->main_model->get_venue_workhours($id);
                $data['dogodki'] = $this->main_model->get_venue_events($id);
                $data['mize'] = $this->main_model->get_tables($id);
                $data['rezervacije'] = $this->main_model->get_reserved_slots($id);
                $data['proste_rezervacije'] = $this->main_model->get_venue_free_slots($id, $data['odpiralni_casi']);
                

                $this->load->view('templates/header');
                $this->load->view('pages/single_venue', $data);
                $this->load->view('templates/footer');
        }

        public function view_venue_edit($id, $section)
        {
                $data['to_be_edited'] = $section;
                $data['id'] = $id;

                if ($section == 'basic_info')
                {
                        $this->form_validation->set_rules('name','Name', 'required');
                        $this->form_validation->set_rules('address','Address', 'required');  
                        $this->form_validation->set_rules('company','Company', 'required');

                        if ($this->form_validation->run() === FALSE)
                        {
                                $data['gostisce'] = $this->main_model->get_venue_info($id);
                                $this->load->view('templates/header');
                                $this->load->view('pages/venue_edit', $data);
                                $this->load->view('templates/footer');
                        }
                        else
                        {
                                $this->main_model->save_edited($id, $section);
                                header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_success");
                                exit();
                        }
                } 
                elseif ($section == 'working_hours')
                {
                        $this->form_validation->set_rules('open','Odprto', 'required');
                        $this->form_validation->set_rules('close','Zaprto', 'required');
                        
                        if ($this->form_validation->run() === FALSE)
                        {
                                $data['odpiralni_casi'] = $this->main_model->get_venue_workhours($id);
                                $this->load->view('templates/header');
                                $this->load->view('pages/venue_edit', $data);
                                $this->load->view('templates/footer');
                        }
                        else
                        {
                                $this->main_model->save_edited($id, $section);
                                header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_success");
                                exit();
                        }
                }
                
        }

        public function view_venue_create($id, $section)
        {
                $data['to_be_edited'] = $section;
                $data['id'] = $id;
                
                if ($section == 'price_list') 
                {
                        $this->form_validation->set_rules('dish','Dish', 'required');
                        $this->form_validation->set_rules('price','Price', 'required');
                        
                }
                elseif ($section == 'events')
                {
                        $this->form_validation->set_rules('name','Name', 'required');
                        $this->form_validation->set_rules('location','Location', 'required');
                        $this->form_validation->set_rules('date','Date', 'required');
                        $this->form_validation->set_rules('description','Description', 'required');
                        $this->form_validation->set_rules('max_guests','Maximum number of guests', 'required');
                        
                }
                elseif ($section == 'tables')
                {
                        $this->form_validation->set_rules('table_num','Number', 'required');
                        $this->form_validation->set_rules('max_guests','Maximum number of guests', 'required');
                        
                }

                if ($this->form_validation->run() === FALSE)
                {
                        $this->load->view('templates/header');
                        $this->load->view('pages/venue_edit', $data);
                        $this->load->view('templates/footer');
                }
                else
                {
                        $this->main_model->create($id, $section);
                        header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_success");
                        exit();    
                }
        }

        public function view_venue_delete($to_delete, $id)
        {
            $this->main_model->delete($to_delete, $id);
            header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_success");
            exit(); 
        }

        public function view_success()
        {
                $this->load->view('templates/header');
                $this->load->view('pages/success');
                $this->load->view('templates/footer');
        }

        public function view_my_profile($role)
        {
            $id = $this->session->userdata('user_id');
            $username = $this->session->userdata('username');
            if($role == 'venue')
            {
                $this->view_single_venue($id);
            }
            else
            {
                $data['rezervacije'] = $this->main_model->get_user_reservations($username);
                $this->load->view('templates/header');
                $this->load->view('pages/normal_user', $data);
                $this->load->view('templates/footer');
            }
        }

        public function view_reserve_slot($st_mize, $ura, $id_gostisca, $confirmed)
        {
            $id_uporabnika = $this->session->userdata('user_id');
            $uporabnisko_ime = $this->session->userdata('username');
            $reservation_data = array(
                'st_mize'         => $st_mize,
                'ura'             => $ura,
                'uporabnisko_ime' => $uporabnisko_ime,
                'id_gostisca'     => $id_gostisca,
            );

            //if ($confirmed == '1')
            if (true)
            {
                $this->main_model->make_reservation($reservation_data);
                header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_success");
                exit(); 
            }
            else
            {
                $this->load->view('templates/header');
                $this->load->view('pages/reservation', $reservation_data);
                $this->load->view('templates/footer');
            }
        }




}