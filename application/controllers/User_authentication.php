<?php
class User_authentication extends CI_Controller {


    public function __construct() 
    {
        parent::__construct();
    
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('user_authentication_model');
        
    }

    public function index() 
    {
        $data['message_display'] = 'Za uporabo spletne aplikacije se prosimo prijavite';
        $this->load->view('templates/header');
        $this->load->view('user_authentication/login_form', $data);
        $this->load->view('templates/footer');
    }

    public function signup($user_role) 
    {
        $data['user_role'] = $user_role;
        $data['message_display'] = 'Vpišite zahtevane podatke nato pa pritisnite gumb Registracija';
        $this->load->view('templates/header');
        $this->load->view('user_authentication/registration_form', $data);
        $this->load->view('templates/footer');
    }

    public function register($user_role) 
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('email_value', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($user_role == 'venue')
        {
            $this->form_validation->set_rules('name','Name', 'required');
            $this->form_validation->set_rules('address','Address', 'required');  
            $this->form_validation->set_rules('company','Company', 'required');
        }

        if ($this->form_validation->run() == FALSE) 
        {
            $data['message_display'] = 'Vpišite zahtevane podatke nato pa pritisnite gumb Registracija';
            $data['error_message'] = 'Nekaterih podatkov niste vnesli pravilno. Poskusite ponovno.';
            $data['user_role'] = $user_role;
            $this->load->view('templates/header');
            $this->load->view('user_authentication/registration_form', $data);
            $this->load->view('templates/footer');
        } 
        else 
        {
            $data_user_login = array(
                'user_name'     => $this->input->post('username'),
                'user_email'    => $this->input->post('email_value'),
                'user_password' => $this->input->post('password'),
                'user_role'     => $user_role
            );

            if ($user_role == 'venue')
            {
                $data_venue = array(
                    'id'              => '',
                    'ime'             => $this->input->post('name'),
                    'naslov'          => $this->input->post('address'),
                    'regija'          => $this->input->post('location'),
                    'hrana'           => $this->input->post('food'),
                    'cena'            => $this->input->post('price'),
                    'naziv_podjetja'  => $this->input->post('company'),
                );
                $data_working_hours = array(
                    'odprto'      => $this->input->post('open'),
                    'zaprto'      => $this->input->post('close'),
                    'id_gostisca' => ''
                );
                $result = $this->user_authentication_model->registration_insert($user_role, $data_user_login, $data_venue, $data_working_hours);
            }
            else
            {
                $result = $this->user_authentication_model->registration_insert($user_role, $data_user_login, array(), array());
            }
            
            if ($result == TRUE) 
            {
                $data['message_display'] = 'Registracija uspešna!';
                $this->load->view('templates/header');
                $this->load->view('user_authentication/login_form', $data);
                $this->load->view('templates/footer');
            } 
            else 
            {
                $this->load->view('templates/header');
                $this->load->view('user_authentication/registration_form');
                $this->load->view('templates/footer');
            }
        }
    }

    public function signin() 
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        $result = $this->user_authentication_model->login($data);
        
        if ($result == TRUE) 
        {
            $username = $this->input->post('username');
            $result = $this->user_authentication_model->read_user_information($username);
            if ($result != false) 
            {
                $session_data = array(
                    'user_id'   => $result[0]->user_id,
                    'username'  => $result[0]->user_name,
                    'email'     => $result[0]->user_email,
                    'role'      => $result[0]->user_role,
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($session_data);
                header("Location: https://www.studenti.famnit.upr.si/~89201099/spletna-stran/index.php/main/view_start");
                exit();
            }
        } 
        else 
        {
            $data = array(
                'message_display' => 'Za uporabo spletne aplikacije se prosimo prijavite',
                'error_message'   => 'Nepravilno uporabniško ime ali geslo!'
            );
            $this->load->view('templates/header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('templates/footer');
        }
    }

    public function logout() 
    {
        $this->session->unset_userdata(array('user_id', 'username', 'email', 'logged_in'));
        $this->session->sess_destroy();
        $data['message_display'] = 'Uspešna odjava';
        $this->load->view('templates/header');
        $this->load->view('user_authentication/login_form', $data);
        $this->load->view('templates/footer');
    }
}
?>